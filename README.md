# Neuropixels

Hold your dovetail probes on holders that fit into 8 mm Stereotaxic equipment! Also holds the headstage and the cables out of the way. Complete with a protective cover and stand for up to 5 probes!

The non-dovetail (flat?) version could work well with this, but you may wish to alter the design to remove the dovetail aspect...

3D printed with microSLA in rigid nylon with glass infill. Don't have that? Order from Jason Rosenberg <j.rosenberg@rosenbergindustries.com> for like 20 bucks!

I'll throw this on my page (bit.ly/onecore) eventually, but wanted to share the designs now and go on vacation.

Youtube instructions: https://youtu.be/UNHbHeN8dg0

Other components: 
* 5 min epoxy for adhearing nuts into holder (and for attaching flat probes to Holder. that's how it's done? I don't know, I have dovetail)
* Nuts M1.4 x .3 mm Thread 1.2 mm thick, McMaster 94150A114 for Dovetail Probe and Cover
* Screws M1.4 x 0.3 mm Thread, 4 mm Long McMaster 92290A472 for Dovetail Probe and Cover
* Screws M1 x 0.25mm Thread, 3mm Long McMaster 91800A052 for PCBA to Holder
* (optional) Nuts M3 McMaster 90592A085 for holding Holders in Stand
* (optional) Screws M3 x 0.35 mm Thread, 6 mm Long McMaster 90751A110 for holding Holders in Stand